#!/bin/sh

plasmapkg2 --type kwinscript --remove forceblury
plasmapkg2 -i forceblur.kwinscript #|| plasmapkg2 -u forceblur.kwinscript
mkdir -pv ~/.local/share/kservices5/
cp -vf metadata.desktop ~/.local/share/kservices5/forceblur.desktop

qdbus org.kde.KWin /Scripting unloadScript "forceblury"
qdbus org.kde.KWin /KWin reconfigure
qdbus org.kde.KWin /Scripting loadScript . "forceblury"
qdbus org.kde.KWin /KWin reconfigure
