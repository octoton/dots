pacman -S stow			# create similink and keep all dotfile in one place
pacman -S powerline		# fancy terminal line

pacman -S yakuake		# dropdown terminal
pacman -S xdotool		# get transparant yakuake
pacman -S xorg-xrandr	# same
mkdir -p ~/.config/autostart
ln -s /usr/share/applications/org.kde.yakuake.desktop ~/.config/autostart/

pacman -S neofetch		# stylish cube info when terminal open

pacman -S conky			# desktop decoration/infos

pacman -S firefox		# navigator

pacman -S latte-dock	# new kde panel

curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh # rust
