RUN in __setup/
	1/ ./arch.sh
RUN in .dotfiles
	1/ __setup/setup.sh
	2/ sudo __setup/setup.sh

For yakuake settings:
	Dolphin Toolbar open terminal: Settings/Applications/Default Appplications/Terminal Emulator -> Yakuake
	More info: https://www.kartar.net/2018/07/working-with-yakuake/


Conky:
	If more than one activity:
		In own_window_hints use decorated to show the bar-> right click more action:
		Special application settings -> Activity -> choose wich activity
		In own_window_hints go back to undecorated
		
	If restore previous session is activated:
		System setting, startup and shutdown, destop session:
			'Application to be ecluded from sessions' -> conky


Firefox:
to get css id and class just see:
	https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox for more info

to stop custom theme just rename the chrome folder in:
	/home/max/.mozilla/firefox/xxx.default/chrome/...


to add when 5.18:
	ui.rc file of dolphin .local/share/kxmlgui5/dolphin #so that dolphin parameters are save like kate


Latte dock: ** Not working **
	hide kde panel: 	more settings: bottom+windows can over
						widht == latte dock without full size windows
	Could be that latte doesn't switch the new Plasma Layout, to correct: right-click/Layout/Manage layout -> import: .config/latte/Plasma.layout.config


WIKI:
	**what file are saving what part of the config**
	bash: 
	bin: 
		bin: bash program
	config: 
		.config: 
		.local: 
			share: 
				kservices5: 
				kxmlgui5: config of kde program (like kate or dolphin)
		.mozilla: config of firefox
	git: git shortcut and co
	misc: 
		shortcut_activity: custom shortcut for going to next activity

