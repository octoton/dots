#!/usr/bin/env bash

# make sure we have pulled in and updated any submodules
git submodule init
git submodule update

# what directories should be installable by all users including the root user
base=(
    bash
)

# folders that should, or only need to be installed for a local user
useronly=(
    git
    bin
    config
)

# run the stow command for the passed in directory ($2) in location $1
stowit() {
    usr=$1
    app=$2
    # -v verbose
    # -R recursive
    # -t target
    stow -v -R -t ${usr} ${app}
}

echo ""
echo "Stowing apps for user: ${whoami}"

# install apps available to local users and root
for app in ${base[@]}; do
    stowit "${HOME}" $app 
done

# install only user space folders
if [[ ! "$(whoami)" = *"root"* ]]; then
	#firefox chrome is a special case
	if [ ! -d ~/.mozilla ]
	then
		firefox					# will create .mozilla/firefox/xxx.default
	fi
	position=$(echo "$PWD")
	if [ ! -d ~/.mozilla/firefox/*.default-release ] # old firefox version
	then
		cd ~/.mozilla/firefox/*.default
	else
		cd ~/.mozilla/firefox/*.default-release
	fi
	DFLT_NAME=$(basename "$PWD")
	cd $position
	mkdir -p ~/.dotfiles/config/.mozilla/firefox/$DFLT_NAME/chrome/
	mv ~/.dotfiles/config/.mozilla/firefox/userChrome.css ~/.dotfiles/config/.mozilla/firefox/$DFLT_NAME/chrome/

	cd ..

	
	#conky startup is a special case
	sudo ln -sf ~/.dotfiles/misc/conky-startup.sh /etc/profile.d/conky.sh # tried with .config/autostart but was not working
	
	#kwin blur for yakuake
	cd ~/.dotfiles/misc/forceblury/
	./bin/pack.sh
	./bin/install.sh
	echo "Do not forget to go in kwin and activate the script => configure and add yakuake on one line"
	echo "Also, Desktop effect: blur"
	
	cd ../..
fi

for app in ${useronly[@]}; do
    if [[ ! "$(whoami)" = *"root"* ]]; then
        stowit "${HOME}" $app 
    fi
done

echo ""
echo "##### ALL DONE"
